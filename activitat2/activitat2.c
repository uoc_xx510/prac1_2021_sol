#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <arpa/inet.h>

#include <sys/socket.h>

/*******************************************************************************/

#define QOTD_TCP_PORT           ( 17 ) 
#define QOTD_BUFFER_SIZE        ( 512 )

/*******************************************************************************/

int main(int argc, char** argv)
{
    char server_message[QOTD_BUFFER_SIZE];
    struct sockaddr_in server_addr;
    int socket_desc;
    int result;
    
    // Check input parameters
    if (argc != 2) {
        printf("Error! Wrong number of parameters.\n");
        exit(1);
    }

    // Clean buffers
    memset(server_message,'\0',sizeof(server_message));

    // Create socket and check for errors
    socket_desc = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_desc < 0) {
        printf("Error! Could not create the socket.\n");
        exit(1);
    }
    
    // Fill server_addr structure with IP and port
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = inet_addr(argv[1]);
    server_addr.sin_port = htons(QOTD_TCP_PORT);
        
    // Send connection request to server
    result = connect(socket_desc, (struct sockaddr*)&server_addr, sizeof(server_addr));
    if (result < 0) {
        printf("Error! Unable to connect.\n");
        exit(1);
    }

    // Receive the server's response
    result = recv(socket_desc, server_message, sizeof(server_message), 0);
    if (result < 0){
        printf("Error! Could not receive the server message\n");
        exit(1);
    }
    
    printf("QOTD message from the server\n: %s\n", server_message);

    // Close the socket
    close(socket_desc);
    
    exit(0);
}

/*******************************************************************************/
