#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>

/*******************************************************************************/

#define QOTD_MAX_MESSAGES       ( 1024 )
#define QOTD_BUFFER_SIZE        ( 512 )

/*******************************************************************************/

int main(int argc, char** argv)
{  
    // Allocate memory to QOTD messages
    char buffer[QOTD_MAX_MESSAGES][QOTD_BUFFER_SIZE];

    // Check input parameters
    if (argc != 2) {
        printf("Error! Wrong number of parameters.\n");
        exit(1);
    }
    
    // Open file in read mode
    FILE * fp;
    fp = fopen(argv[1], "r");
    if (fp == NULL) {
        printf("Error! Could not open the file.\n");
        exit(1);
    }

    // Pointer to the current QOTD message
    char* message_ptr;
    int message_counter = 0;

    // Point to the first QOTD message
    message_ptr = buffer[message_counter];

    // Iterate until finished
    bool finished = false;
    do {
        char * line = NULL;
        size_t line_length = 0; 
        ssize_t read;

        // Read a line from the file
        // This will allocate memory using malloc
        read = getline(&line, &line_length, fp);

        // Check if it is the end of the file
        if (read < 0) {
            // Free the memory
            free(line);

            // Declare as finished
            finished = true;

            break;
        }

        // Check if this is the end of a QOTD
        if (line[0] == '%') {
            // Update the number of QOTD messages
            message_counter += 1;
            // Update pointer to the QOTD message
            message_ptr = buffer[message_counter];
        } else {
            // Copy the line to the QOTD message
            // Make sure that we do not overflow by limiting to QOTD_BUFFER_SIZE
            // If the message is longer it will be cut
            strncpy(message_ptr, line, QOTD_BUFFER_SIZE);
            
            // Update the message pointer to keep writing at the end of it
            // in the next iteration of the loop
            message_ptr += read;
        }

        // Free the memory allocated by getline
        free(line);
    } while(!finished);
    
    // Close the file descriptor
    fclose(fp);

    // Ensure that we have read all the messages
    for (int i = 0; i < message_counter; i++) {
        int length;
        char* message_ptr = buffer[i];
        length = strlen(message_ptr);
        printf("message=%d, length=%d\n", i, length);
    }

    exit(0);
}

/*******************************************************************************/
