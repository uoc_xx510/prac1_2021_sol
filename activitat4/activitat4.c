#include <netdb.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

/*******************************************************************************/

#define QOTD_TCP_PORT           ( 17 ) 
#define QOTD_BUFFER_SIZE        ( 28 )

/*******************************************************************************/

int main(int argc, char** argv) {
    int server_sock_fd, client_sock_fd;
    struct sockaddr_in server_addr, client_addr;
    char buffer[QOTD_BUFFER_SIZE];
    int buffer_length = QOTD_BUFFER_SIZE;
    int result;
    int length;

    // Fill in the buffer with letters a-z
    for (uint32_t i = 0; i < QOTD_BUFFER_SIZE; i++) {
        buffer[i] = 97 + i;
    }
    // Fill in the buffer with 
    buffer[26] = '\r';
    buffer[27] = '\n';
  
    // Create a TCP socket
    server_sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_sock_fd < 0) {
        printf("Error! Socket creation failed.\n");
        exit(0);
    }
  
    // Initialize the servaddr structure
    // Allow connections from INADDR_ANY to the QTOD TCP port (17)
    bzero(&server_addr, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons(QOTD_TCP_PORT);
  
    // Bind the socket to the servaddr structure
    result = bind(server_sock_fd, (struct sockaddr *) &server_addr, sizeof(server_addr));
    if (result < 0) {
        printf("Error! Could not bind the socket.\n");
        exit(0);
    }

    // Now server is ready to listen
    result = listen(server_sock_fd, 1);
    if (result < 0) {
        printf("Error! Socket listen failed.\n");
        exit(0);
    }

    bool finished = false;
    while (!finished) {   
        // Accept the connection from the client and return new file descriptor
        client_sock_fd = accept(server_sock_fd, (struct sockaddr *) &client_addr, &length);
        if (client_sock_fd < 0) {
            printf("Error! Socket accept failed.\n");
            exit(0);
        }

        // Write the buffer to the client
        write(client_sock_fd, buffer, buffer_length);

        // Close the client socket
        close(client_sock_fd);
    }
  
    // After chatting close the socket
    close(server_sock_fd);    
}
