#include <fcntl.h>
#include <netdb.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>

#include <arpa/inet.h>
#include <netinet/in.h>

/*******************************************************************************/

#define QOTD_TCP_PORT           ( 17 ) 
#define QOTD_MAX_MESSAGES       ( 1024 )
#define QOTD_BUFFER_SIZE        ( 512 )

#define QTOD_LOG_FILE           ( "activitat5.log" )
#define QTOD_LOG_BUFFER_SIZE    ( 1024 )
#define QTOD_LOG_BUFFER_FMT     ( "TIME=%d, IP=%s, PORT=%d\n" )

/*******************************************************************************/

// Define quotd_msg as a data type that is a char array of size QOTD_BUFFER_SIZE
typedef char (quotd_msg_t)[QOTD_BUFFER_SIZE];

// Define quotd_buffer_t as a data type that is a quotd_msg_t array of size QOTD_MAX_MESSAGES
typedef quotd_msg_t (quotd_buffer_t)[QOTD_MAX_MESSAGES];

/*******************************************************************************/

bool read_quotd_file(char* file_name, quotd_buffer_t* quotd_buffer_ptr, int* qotd_messages) { 
    // Open file in read mode
    FILE * fp;
    fp = fopen(file_name, "r");
    if (fp == NULL) {
        return false;
    }

    // Pointer to the current QOTD message
    char * message_ptr;
    int message_counter = 0;

    // Point to the first QOTD message
    message_ptr = (*quotd_buffer_ptr)[message_counter];

    // Iterate until finished
    bool finished = false;
    do {
        char * line = NULL;
        size_t line_length = 0; 
        ssize_t read;

        // Read a line from the file
        // This will allocate memory using malloc
        read = getline(&line, &line_length, fp);

        // Check if it is the end of the file
        if (read < 0) {
            // Free the memory
            free(line);

            // Declare as finished
            finished = true;

            break;
        }

        // Check if this is the end of a QOTD
        if (line[0] == '%') {
            // Update the number of QOTD messages
            message_counter += 1;
            // Update pointer to the QOTD message
            message_ptr = (*quotd_buffer_ptr)[message_counter];
        } else {
            // Copy the line to the QOTD message
            // Make sure that we do not overflow by limiting to QOTD_BUFFER_SIZE
            // If the message is longer it will be cut
            strncpy(message_ptr, line, QOTD_BUFFER_SIZE);
            
            // Update the message pointer to keep writing at the end of it
            // in the next iteration of the loop
            message_ptr += read;
        }

        // Free the memory allocated by getline
        free(line);
    } while(!finished);
    
    // Close the file descriptor
    fclose(fp);

    // Update the number of quotes that have been read
    *qotd_messages = message_counter;

    return true;
}

/*******************************************************************************/

int main(int argc, char** argv)
{  
    FILE * log_fp;
    struct timeval tv;
    struct sockaddr_in server_addr, client_addr;
    int server_sock_fd, client_sock_fd;
    int result, length;
    bool status;

    // Allocate memory to QOTD messages
    quotd_buffer_t qotd_buffer;
    int qotd_messages = 0;
    
    // Get time of day
    gettimeofday(&tv, NULL);

    // Seed the random number generator
    srand(tv.tv_sec);

    // Open log file in append mode
    log_fp = fopen(QTOD_LOG_FILE, "a+");
    if (log_fp == NULL) {
        printf("Error! Could not open log file.\n");
        exit(1);
    }

    // Check input parameters
    if (argc != 2) {
        printf("Error! Wrong number of parameters.\n");
        exit(1);
    }
    
    // Open and read the QOTD file to a memory structure
    status = read_quotd_file(argv[1], &qotd_buffer, &qotd_messages);
    if (!status) {
        printf("Error! Could not open or parse the file.\n");
    }

    // Create a TCP socket
    server_sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_sock_fd < 0) {
        printf("Error! Socket creation failed.\n");
        exit(0);
    }
  
    // Initialize the servaddr structure
    // Allow connections from INADDR_ANY to the QTOD TCP port (17)
    bzero(&server_addr, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons(QOTD_TCP_PORT);
  
    // Bind the socket to the servaddr structure
    result = bind(server_sock_fd, (struct sockaddr *) &server_addr, sizeof(server_addr));
    if (result < 0) {
        printf("Error! Could not bind the socket.\n");
        exit(1);
    }

    // Now server is ready to listen
    result = listen(server_sock_fd, 1);
    if (result < 0) {
        printf("Error! Socket listen failed.\n");
        exit(1);
    }

    bool finished = false;
    while (!finished) {   
        char log_buffer[QTOD_LOG_BUFFER_SIZE];
        char client_ip_address[32];
        uint32_t rnd_number;

        // Accept the connection from the client and return new file descriptor
        client_sock_fd = accept(server_sock_fd, (struct sockaddr *) &client_addr, &length);
        if (client_sock_fd < 0) {
            printf("Error! Socket accept failed.\n");
            exit(1);
        }

        // Get time of day for logging
        gettimeofday(&tv, NULL);

        // Convert IP address to string 
        inet_ntop(AF_INET, &(client_addr.sin_addr), client_ip_address, INET_ADDRSTRLEN);

        // Print the log message to the log file and ensure it is written
        fprintf(log_fp, QTOD_LOG_BUFFER_FMT, (uint32_t) tv.tv_sec, client_ip_address, client_addr.sin_port);
        fflush(log_fp);

        // Generate a random number and limit the value to the number of QOTD messages
        rnd_number = random() % qotd_messages;

        // Create a pointer to the QOTD message and count its size
        char* buffer = qotd_buffer[rnd_number];
        uint32_t buffer_length = strlen(buffer);

        // Write the buffer to the client
        write(client_sock_fd, buffer, buffer_length);

        // Close the client socket
        close(client_sock_fd);
    }
  
    // After chatting close the socket
    close(server_sock_fd);    

    // Close the file descriptor
    fclose(log_fp);

    exit(0);
}

/*******************************************************************************/
